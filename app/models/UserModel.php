<?php
class UserModel extends Db
{
    // Get all users
    function getAllUsers()
    {
        $sql = self::$connection->prepare("SELECT * FROM users");
        $sql->execute(); //return an object
        $items = array();
        $items = $sql->get_result()->fetch_all(MYSQLI_ASSOC);
        return $items; //return an array
    }
    // Get top Player
    function getTopPlayer()
    {
        $sql = self::$connection->prepare("SELECT * FROM `users` 
        ORDER BY `hightestScore` DESC");
        $sql->execute();
        $items = array();
        $items = $sql->get_result()->fetch_all(MYSQLI_ASSOC);
        return $items; //return an array
    }

    // Get Index of User in Rank
    function getIndexOfUser($token)
    {
        $result = new Result();
        try {
            $user = $this->getProfile($token);
            if (!$user->isSuccess) {
                $result->setError($user->error);
            } else {
                // Find Index
                $topUsers = $this->getTopPlayer();
                $index = 0;
                foreach ($topUsers as $user) {
                    $index++;
                    if ($user['token'] == $token) {
                        $result->setData($index);
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }

    // Buy Arrow
    function buyArrow($token, $arrowNumber)
    {
        $result = new Result();
        try {
            $user = $this->getProfile($token);
            if (!$user->isSuccess) {
                $result->setError($user->error);
            } else {
                // Find Index
                $users = $this->getAllUsers();
                foreach ($users as $user) {
                    if ($user['token'] == $token) {
                        switch ($arrowNumber) {
                            case 1:
                                if ($user['accumulatedScore'] >= 10) {
                                    $newArrowNumber = $user['arrowNumber'] + 1;
                                    $newAccumulatedScore = $user['accumulatedScore'] - 10;
                                    // SQL
                                    $sql = self::$connection->prepare("UPDATE `users` 
                                    SET `accumulatedScore`= ?,
                                        `arrowNumber`= ?
                                    WHERE `token`= ?");
                                    $sql->bind_param('iis', $newAccumulatedScore, $newArrowNumber, $token);
                                    $sql->execute();
                                    // Set Data
                                    $result->setData([
                                        "newArrowNumber" => $newArrowNumber,
                                        "newAccumulatedScore" => $newAccumulatedScore
                                    ]);
                                } else {
                                    $result->setError('You don\'t have enough accumulated score to buy ' . $arrowNumber . ' arrow');
                                }
                                break;
                            case 3:
                                if ($user['accumulatedScore'] >= 20) {
                                    $newArrowNumber = $user['arrowNumber'] + 3;
                                    $newAccumulatedScore = $user['accumulatedScore'] - 20;
                                    // SQL
                                    $sql = self::$connection->prepare("UPDATE `users` 
                                    SET `accumulatedScore`= ?,
                                        `arrowNumber`= ?
                                    WHERE `token`= ?");
                                    $sql->bind_param('iis', $newAccumulatedScore, $newArrowNumber, $token);
                                    $sql->execute();
                                    $sql->execute();
                                    // Set Data
                                    $result->setData([
                                        "newArrowNumber" => $newArrowNumber,
                                        "newAccumulatedScore" => $newAccumulatedScore
                                    ]);
                                } else {
                                    $result->setError('You don\'t have enough accumulated score to buy ' . $arrowNumber . ' arrow');
                                }
                                break;
                            case 10:
                                if ($user['accumulatedScore'] >= 60) {
                                    $newArrowNumber = $user['arrowNumber'] + 10;
                                    $newAccumulatedScore = $user['accumulatedScore'] - 60;
                                    // SQL
                                    $sql = self::$connection->prepare("UPDATE `users` 
                                    SET `accumulatedScore`= ?,
                                        `arrowNumber`= ?
                                    WHERE `token`= ?");
                                    $sql->bind_param('iis', $newAccumulatedScore, $newArrowNumber, $token);
                                    $sql->execute();
                                    $sql->execute();
                                    // Set Data
                                    $result->setData([
                                        "newArrowNumber" => $newArrowNumber,
                                        "newAccumulatedScore" => $newAccumulatedScore
                                    ]);
                                } else {
                                    $result->setError('You don\'t have enough accumulated score to buy ' . $arrowNumber . ' arrow');
                                }
                                break;

                            default:
                                $result->setError('Some thing wrong at arrow number');
                                break;
                        }
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Register 
    function register($fullname, $email, $password, $rePassword, $gender)
    {
        $result = new Result();
        try {
            if ($this->checkEmailExit($email)) {
                $result->setError('This email have been used');
            } else if ($password != $rePassword) {
                $result->setError('Password and Confirm Password is not same');
            } else {
                $deactiveCode = $this->RandomString(25);
                $password = md5($password);
                $sql = self::$connection->prepare("
                INSERT INTO `users`(`fullname`, `email`, `password`, `gender`, `deactiveCode`) 
                VALUES (?,?,?,?,?)");
                $sql->bind_param('sssis', $fullname, $email, $password, $gender, $deactiveCode);
                $sql->execute();
                // Find that User
                foreach ($this->getAllUsers() as $user) {
                    if ($user['email'] == $email) {
                        $result->setData($user);
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Login
    function login($email, $password)
    {
        $result = new Result();
        try {
            $result->setError('Wrong at Email or Password');
            $users = $this->getAllUsers();
            foreach ($users as $user) {
                if ($user['email'] == $email && $user['password'] == md5($password)) {
                    if (is_null($user['deactiveCode']) != 1) {
                        $result->setError('This Account is not confirm');
                    } else {
                        $token = $this->RandomString(25);
                        $sql = self::$connection->prepare("UPDATE `users` 
                        SET `token`='" . $token . "' 
                        WHERE `email`='" . $email . "'");
                        $sql->execute();
                        $result->setData([
                            "token" => $token,
                            "userName" => $user['fullname']
                        ]);
                    }
                    break;
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Get User by token
    function getProfile($token)
    {
        $result = new Result();
        try {
            $result->setError('Cann\'t find any User');
            foreach ($this->getAllUsers() as $user) {
                if ($user['token'] == $token) {
                    $result->setData($user);
                    break;
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Update Score
    function updateScore($token, $score, $arrowNumber)
    {
        $result = new Result();
        try {
            $check = $this->getProfile($token);
            if (!$check->isSuccess) {
                $result->setError($check->error);
            } else {
                $data = $check->data;
                $newAccumulatedScore = $data['accumulatedScore'] + $score;
                $newHightScore = $data['hightestScore'] < $score ? $score : $data['hightestScore'];
                $sql = self::$connection->prepare("UPDATE `users`
                SET `accumulatedScore`= ?,
                    `hightestScore`= ?,
                    `arrowNumber`= ?
                WHERE `token` = ?");
                $sql->bind_param('iiis', $newAccumulatedScore, $newHightScore, $arrowNumber, $token);
                $sql->execute();
                $result->setData('Update Score Success');
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Active Account
    function activeAccount($deactiveCode)
    {
        $result = new Result();
        try {
            $check = false;
            $id = 0;
            foreach ($this->getAllUsers() as $user) {
                if ($user['deactiveCode'] == $deactiveCode) {
                    $check = true;
                    $id = $user['id'];
                    break;
                }
            }
            if (!$check) {
                $result->setError('Cann\'t find any User');
            } else {
                $sql = self::$connection->prepare("UPDATE `users` 
                SET `deactiveCode`= null 
                WHERE `id` = " . $id);
                // $sql->bind_param('i', $id);
                $sql->execute();
                foreach ($this->getAllUsers() as $user) {
                    if ($user['id'] == $id) {
                        $result->setData($user);
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Create Reset Password Code
    function createResetPasswordCode($email)
    {
        $result = new Result();
        try {
            if (!$this->checkEmailExit($email)) {
                $result->setError('Cannot find any User have this email');
            } else {
                $resetCode = $this->RandomString(8);
                $sql = self::$connection->prepare("UPDATE `users` 
                SET `resetPasswordCode`= \"" . $resetCode . "\" 
                WHERE `email` = \"" . $email . "\" ");
                $sql->execute();
                foreach ($this->getAllUsers() as $user) {
                    if ($user['resetPasswordCode'] == $resetCode) {
                        $result->setData($user);
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    // Active Account
    function resetPasswordByCode($resetCode,$password,$rePassword)
    {
        $result = new Result();
        try {
            $check = false;
            $id = 0;
            foreach ($this->getAllUsers() as $user) {
                if ($user['resetPasswordCode'] == $resetCode) {
                    $check = true;
                    $id = $user['id'];
                    break;
                }
            }
            if (!$check) {
                $result->setError('Opps, some thing wrong at this code');
            }else if($password != $rePassword){
                $result->setError('Password and Confirm Password is not same !!');
            }
             else {
                 $password = md5($password);
                $sql = self::$connection->prepare("UPDATE `users` 
                SET `password`= ?,
                    `resetPasswordCode` = null
                WHERE `id` = ? ");
                $sql->bind_param('si', $password,$id);
                $sql->execute();
                $result->setData('Update Password Success !');
            }
        } catch (Exception $ex) {
            $result->setError($ex->getMessage());
        }
        return $result;
    }
    //Check Email Exist
    function checkEmailExit($email)
    {
        $users = $this->getAllUsers();
        foreach ($users as $user) {
            if ($user['email'] == $email) {
                return true;
            }
        }
        return false;
    }
    // Random String
    public function RandomString($stringsLength)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randString = '';
        for ($i = 0; $i < $stringsLength; $i++) {
            $randString .= $characters[rand(0, strlen($characters)) - 1];
        }
        return $randString;
    }
}

class Result
{
    public $isSuccess, $data, $error;
    public function __construct()
    {
        $this->isSuccess = false;
        $this->data = null;
        $this->error = "Don't have Value";
    }
    // Set Data
    public function setData($data)
    {
        $this->isSuccess = true;
        $this->data = $data;
        $this->error = null;
    }
    // Set Data
    public function setError($error)
    {
        $this->isSuccess = false;
        $this->data = null;
        $this->error = $error;
    }
}
