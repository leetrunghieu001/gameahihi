<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forget Password</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="./public/css/login.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <div class="background">
        <div class="main">
            <h1 class="dangnhap">Forget Password</h1>
            <h3 style="color: white;">Enter Your Email, which Registered and press "Get code" button to get Code Reset Password</h3>
            <form class="form-reset-password">
                <div class="dangnhap-input">
                    <i class="fas fa-user-tie fa-4x"></i>
                    <input class="input-dangnhap email-input" placeholder="Email" type="email" name="email" autocomplete="off" required>
                </div>
                <div class="submit-dangnhap">
                    <button class="submit btn-submit" type="submit">GET CODE</button>
                </div>
            </form>
            <div class="submit-dangki">
                <a href="./login.php">
                    <button class="submit">LOGIN</button>
                </a>
            </div>
        </div>
    </div>
    <form action="./server/sendMailResetPassword.php" method="POST" style="display: none;">
        <input type="text" class="email-hidden" name="email">
        <input type="text" class="fullname-hidden" name="fullname">
        <input type="text" class="reset-code-hidden" name="code">
        <button type="submit" class="btn-submit-hidden" name="resetPassSubmit"></button>
    </form>
    <div class="loader">
        <div class="load"></div>
    </div>
    <script src="./public/js/checkNotLogin.js"></script>
    <script src="./public/js/forgetPassword.js"></script>
</body>

</html>