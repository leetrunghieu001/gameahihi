<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hiragana</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <!-- Font-awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <!-- CSS alertify -->
    <link rel="stylesheet" href="./public/css/alertify/alertify.min.css" />
    <!-- Default theme -->
    <link rel="stylesheet" href="./public/css/alertify/default.min.css" />
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="./public/css/alertify/semantic.min.css" />
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="./public/css/alertify/bootstrap.min.css" />
    <link rel="stylesheet" href="./public/css/hieu_cao.css">
    <?php if (!isset($_GET['newbie'])) {
    ?>
        <style>
            .text-vietnamese {
                display: none;
            }
        </style>
        <?php } else {
        if ($_GET['newbie'] != 'true') { ?><style>
                .text-vietnamese {
                    display: none;
                }
            </style>
    <?php }
    } ?>
</head>

<body>
    <div class="container">
        <!-- User info -->
        <div class="user-info">
                <div class="user-name">Hello "<b></b>"</div>
                <div class="user-hightest-score">Your Hight Score "<b></b>"</div>
        </div>
        <!-- Left row (Toolbar) -->
        <div class="left-row">
            <div class="toolbar-properties">
                <div class="score-information">
                    <div>Score: <span class="score">0</span></div>
                    <div>Spam: <span class="spam">0</span></div>
                    <div>Lever: <span class="lever">0</span></div>
                </div>
                <div class="toolbar-arrow">
                    <div class="arrow-icon">
                        <button class="arrow-button"></button>
                    </div>
                    <div>&#127993; x <span class="arrow-number">5</span></div>
                </div>
            </div>
        </div>
        <!-- Left row -->
        <div class="right-row">
            <div class="keyboard-shortcuts">
                <h3>Keyboard Shortcuts</h3>
                <b>Backspace : </b><br><span>Reset Word</span><br>
                <b>Space : </b><br><span>Click into arrow icon</span><br>
                <b>Enter : </b><br><span>Turn on/off background sound</span>
            </div>
        </div>
        <!-- Message -->
        <div class="message"></div>
        <!-- Content -->
        <div class="balloon-zone"></div>


        <!-- Screen Message -->
        <div class="screen-message">
            <div></div>
        </div>
        <!-- Pronounce the word -->
        <audio id="pronounce" autoplay>
            <source src="" type="audio/mpeg">
        </audio>
        <!-- Arrow Sound -->
        <audio id="arrow-sound" autoplay>
            <source src="./public/audio/arrow/Arrow-Sound.mp3" type="audio/mpeg">
        </audio>
        <!-- Background Sound -->
        <audio id="background-sound" autoplay loop>
            <source src="./public/audio/background-sound.mp3" type="audio/mpeg">
        </audio>
        <!-- Game Over Sound -->
        <audio id="game-over-sound" autoplay>
            <source src="./public/audio/game-over.mp3" type="audio/mpeg">
        </audio>
    </div>
    <!-- Loader -->
    <div class="loader"><div class="load"></div></div>
    <!-- Alertify -->
    <script src="./public/js/alertify/alertify.min.js"></script>
    <!-- Custom JS file -->
    <script src="./public/js/hieu_cao.js"></script>
</body>

</html>