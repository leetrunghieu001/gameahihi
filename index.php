<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Menu Game</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Luckiest+Guy|Orbitron|Russo+One|Shadows+Into+Light|Teko&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./public/css/menuGame.css">
</head>

<body>
  <div class="hello">Welcome back "<i></i>"</div>
  <div class="frame1">
    <img src="./public/image/menu-game-title.png">
  </div>
  <div class="frame">
    <div class="btn-group">
      <button class="button" type="button" onclick="btnPress('newbie')">
        <h2>NEWBIE</h2>
      </button><br>
      <button class="button" type="button" onclick="btnPress('pro')">
        <h2>PRO PLAYER</h2>
      </button><br>
      <button class="button" type="button" onclick="btnPress('buy')">
        <h2>BUY ARROW</h2>
      </button><br>
      <button class="button" type="button" onclick="btnPress('rank')">
        <h2>TOP PLAYER</h2>
      </button>
      <button class="button" type="button" onclick="btnPress('logout')">
        <h2>LOGOUT</h2>
      </button>
    </div>
  </div>
  <!-- Loader -->
  <div class="loader"><div class="load"></div></div>
  <script src="./public/js/menuGame.js"></script>
</body>

</html>