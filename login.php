<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="./public/css/login.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <div class="background">
        <div class="main">
            <h1 class="dangnhap">Đăng Nhập</h1>
            <form action="#" method="POST">
            <div class="dangnhap-input">
                <i class="fas fa-user-tie fa-4x"></i>
                <input class="input-dangnhap email-input" placeholder="Email" type="email" name="email"
                    autocomplete="off" required>
            </div>
            <div class="dangnhap-input">
                <i class="fas fa-lock fa-4x"></i>
                <input class="input-dangnhap password-input" placeholder="Password" type="password" name="password"
                    autocomplete="off" required>

            </div>
            <div class="submit-dangnhap">
                <button class="submit btn-submit" type="submit">Đăng Nhập</button>
            </div>
        </form>
            <div class="submit-dangki">
                <a href="./register.php">
                    <button class="submit">Đăng Kí</button>
                </a>
            </div>
            <!-- Forget Password -->
            <div class="submit-dangki">
                <a href="./forgetPassword.php">
                    <button class="submit">Forget Password ?</button>
                </a>
            </div>
        </div>
    </div>
    <div class="loader"><div class="load"></div></div>
    <script src="./public/js/checkNotLogin.js"></script>
    <script src="./public/js/login.js"></script>
</body>

</html>