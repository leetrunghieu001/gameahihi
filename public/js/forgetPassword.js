const form = document.querySelector('.form-reset-password');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    resetPassword();
});
async function resetPassword() {
    let loader = document.querySelector('.loader');
    loader.style.display = 'initial';
    const email = document.querySelector('.email-input').value;
    // Sent Data to Sever
    const data = {
        email: email
    };
    const url = './server/forgetPassword.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        alert(`Please your email to get reset Code`);
        let emailHidden = document.querySelector('.email-hidden');
        let fullnameHidden = document.querySelector('.fullname-hidden');
        let resetCodeHidden = document.querySelector('.reset-code-hidden');
        let btnSubmidHidden = document.querySelector('.btn-submit-hidden');
        emailHidden.value = result.data.email;
        fullnameHidden.value = result.data.fullname;
        resetCodeHidden.value = result.data.resetPasswordCode;
        btnSubmidHidden.click();
    } else {
        alert(result.error);
    }
    // Close loader
    loader.style.display = 'none';
}