const arr = [
    { "japanese": "あ", "vietnamese": "a" },
    { "japanese": "い", "vietnamese": "i" },
    { "japanese": "う", "vietnamese": "u" },
    { "japanese": "え", "vietnamese": "e" },
    { "japanese": "お", "vietnamese": "o" },
    { "japanese": "か", "vietnamese": "ka" },
    { "japanese": "き", "vietnamese": "ki" },
    { "japanese": "く", "vietnamese": "ku" },
    { "japanese": "け", "vietnamese": "ke" },
    { "japanese": "こ", "vietnamese": "ko" },
    { "japanese": "さ", "vietnamese": "sa" },
    { "japanese": "し", "vietnamese": "shi" },
    { "japanese": "す", "vietnamese": "su" },
    { "japanese": "せ", "vietnamese": "se" },
    { "japanese": "そ", "vietnamese": "so" },
    { "japanese": "た", "vietnamese": "ta" },
    { "japanese": "ち", "vietnamese": "chi" },
    { "japanese": "つ", "vietnamese": "tsu" },
    { "japanese": "て", "vietnamese": "te" },
    { "japanese": "と", "vietnamese": "to" },
    { "japanese": "な", "vietnamese": "na" },
    { "japanese": "に", "vietnamese": "ni" },
    { "japanese": "ぬ", "vietnamese": "nu" },
    { "japanese": "ね", "vietnamese": "ne" },
    { "japanese": "の", "vietnamese": "no" },
    { "japanese": "は", "vietnamese": "ha" },
    { "japanese": "ひ", "vietnamese": "hi" },
    { "japanese": "ふ", "vietnamese": "fu" },
    { "japanese": "へ", "vietnamese": "he" },
    { "japanese": "ほ", "vietnamese": "ho" },
    { "japanese": "ま", "vietnamese": "ma" },
    { "japanese": "み", "vietnamese": "mi" },
    { "japanese": "む", "vietnamese": "mu" },
    { "japanese": "め", "vietnamese": "me" },
    { "japanese": "も", "vietnamese": "mo" },
    { "japanese": "や", "vietnamese": "ya" },
    { "japanese": "ゆ", "vietnamese": "yu" },
    { "japanese": "よ", "vietnamese": "yo" },
    { "japanese": "ら", "vietnamese": "ra" },
    { "japanese": "り", "vietnamese": "ri" },
    { "japanese": "る", "vietnamese": "ru" },
    { "japanese": "れ", "vietnamese": "re" },
    { "japanese": "ろ", "vietnamese": "ro" },
    { "japanese": "わ", "vietnamese": "wa" },
    { "japanese": "を", "vietnamese": "wo" },
    { "japanese": "ん", "vietnamese": "n" },
    { "japanese": "零", "vietnamese": "rei" },
    { "japanese": "一", "vietnamese": "ichi" },
    { "japanese": "二", "vietnamese": "ni" },
    { "japanese": "三", "vietnamese": "san" },
    { "japanese": "四", "vietnamese": "yon" },
    { "japanese": "五", "vietnamese": "go" },
    { "japanese": "六", "vietnamese": "roku" },
    { "japanese": "七", "vietnamese": "nana" },
    { "japanese": "七", "vietnamese": "shichi" },
    { "japanese": "八", "vietnamese": "hachi" },
    { "japanese": "九", "vietnamese": "kyuu" },
    { "japanese": "十", "vietnamese": "juu" },
];
const leverPropertiesArray = [
    { "lever": "0", "groundHeight": "90%", "ballonNumber": 2, 'balloonSpellMin': 25, 'balloonSpellMax': 30, "ballonNumberPassLever": 3 },
    { "lever": "1", "groundHeight": "90%", "ballonNumber": 3, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 4 },
    { "lever": "2", "groundHeight": "85%", "ballonNumber": 4, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 5 },
    { "lever": "3", "groundHeight": "85%", "ballonNumber": 5, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 6 },
    { "lever": "4", "groundHeight": "80%", "ballonNumber": 6, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 7 },
    { "lever": "5", "groundHeight": "80%", "ballonNumber": 7, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 8 },
    { "lever": "6", "groundHeight": "75%", "ballonNumber": 8, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 9 },
    { "lever": "7", "groundHeight": "75%", "ballonNumber": 8, 'balloonSpellMin': 20, 'balloonSpellMax': 25, "ballonNumberPassLever": 9 },
    { "lever": "8", "groundHeight": "80%", "ballonNumber": 9, 'balloonSpellMin': 20, 'balloonSpellMax': 25 }
];
let strings = "";
let spam = 0;
let score = 0;
let lever = 0;
let arrayPosition = [];
let gameStatus = 'playing';
let bgSound = false;
let balloonExploded = 0;
let arrowNumber = 0;
let setGameOverCall = 0;
// Get Data
window.onload = () => {
    let loader = document.querySelector('.loader');
    loader.style.display = 'inline';
    if (localStorage.getItem("_token") === null) {
        alert('Please Login First');
        logout();
    }
    // Get data
    getProfile()
        .then((result) => {
            if (typeof (result) == 'object') {
                let userName = document.querySelector('.user-name b');
                userName.innerHTML = result.fullname;
                let hightScore = document.querySelector('.user-hightest-score b');
                hightScore.innerHTML = result.hightestScore;
                arrowNumber = result.arrowNumber;
                setArrowNumber();
            }
        });
    // Setup lever for the first time
    setUplever();
    loader.style.display = 'none';
}
// Get Profile
async function getProfile() {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
    };
    const url = './server/getProfile.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    // console.log(result);
    if (result.isSuccess) {
        // Get profile
        return result.data
    } else {
        alert(result.error);
        logout();
    }
    return false;
}
function logout() {
    localStorage.removeItem("_token");
    alert('You were logout');
    window.location.href = './login.php';
}
// Event for keyboard
document.addEventListener('keyup', (e) => {
    if (gameStatus == 'playing' || gameStatus == 'prepareTransferLever') {
        if (e.keyCode >= 65 && e.keyCode <= 90) {
            strings += e.key.toLowerCase();
            setMessage();
            // Check Character same in a balloon
            setTimeout(() => {
                checkCharacterExist()
            }, 400);
            // Check Spam
            if (strings.length >= 7) {
                strings = "";
                setMessage();
                spam++;
                setSpam();
                alertify.warning(`You were spam ${spam} time`);
            }
            if (spam >= 3) {
                setGameOver('No, Don\'t Spam Like That');
            }
        }
        // Backspace button
        if (e.keyCode == 8) {
            strings = "";
            setMessage();
        }
        // Space button is click into arrow icon
        if (e.keyCode == 32) {
            const arrowButton = document.querySelector('.arrow-button');
            arrowButton.click();
        }
        // Enter Button is turn on/off bg music
        if (e.keyCode == 13) {
            bgSound = bgSound ? false : true;
            const backgroundSound = document.querySelector('#background-sound');
            if (bgSound) {
                backgroundSound.autoplay = true;
                backgroundSound.volume = 0.5;
                backgroundSound.load();
            } else {
                backgroundSound.autoplay = false;
                backgroundSound.load();
            }
        }
    }
    if (gameStatus == "over") {
        strings = 'You lost';
        setMessage();
    }
});
// Setup lever 
function setUplever() {
    // Create Balloons
    let balloonsZone = document.querySelector('.balloon-zone');
    for (let i = 0; i < leverPropertiesArray[lever].ballonNumber; i++) {
        balloonsZone.innerHTML += `
    <div class="balloon balloon-animation" id="${i}"></div>`
    }
    // Array balloons
    let balloons = document.querySelectorAll('.balloon');
    let distanceBalloons = 0;
    // Set information for all balloons in the first time
    for (let i = 0; i < balloons.length; i++) {
        // Create Number Position Character In Array Character
        let num = randomInteger(0, arr.length - 1);
        // Only get the value if this number is not same in arrayPosition
        while (checkSameNumber(num)) {
            num = randomInteger(0, arr.length - 1);
        }
        arrayPosition.push(num);
        //  Set distance for all Balloons
        balloons[i].style.left = `calc(11% + ${distanceBalloons}px)`;
        distanceBalloons += balloons[i].offsetWidth + 10;
        // Set animation time for all balloon
        const min = leverPropertiesArray[lever].balloonSpellMin;
        const max = leverPropertiesArray[lever].balloonSpellMax;
        balloons[i].style.animationDelay = randomInteger(1, 4) + 's';
        balloons[i].style.animationDuration = randomInteger(min, max) + 's';
        // Set random black balloon
        setRandomTypeBalloon(balloons[i], num);
        // Set events for that balloon
        balloons[i].onmouseleave = () => {
            balloons[i].style.boxShadow = '';
        };
        balloons[i].onclick = () => {
            const arrowButton = document.querySelector('.arrow-button');
            if (arrowButton.classList.contains('selected')) {
                if (gameStatus == 'playing' ||
                    gameStatus == 'prepareTransferLever') {
                    // Subtract the number of arrows
                    arrowNumber--;
                    setArrowNumber();
                    balloonBurst(balloons[i]);
                    // Reset Style button Arrow
                    arrowButton.classList.remove('selected');
                    arrowButton.style.backgroundColor = 'Transparent';
                    arrowButton.style.border = 'none';
                    // Set sound
                    const audio = document.querySelector("#arrow-sound");
                    audio.querySelector('source').src = `./public/audio/arrow/Arrow-Fly.mp3`;
                    audio.autoplay = true;
                    audio.volume = 0.5;
                    audio.load();
                }
            }
        }
    }
    // Set Ground Height
    const ground = document.querySelector('.message');
    ground.style.top = leverPropertiesArray[lever].groundHeight;
    // Function Inspect
    const isMiss = setInterval(() => {
        if (checkMissBalloon()) {
            // Set Error and game Status
            const error = 'You were miss a balloon !!';
            setGameOver(error);
            if (gameStatus == 'over') {
                // Stop all balloon's animation
                balloons.forEach(balloon => {
                    balloon.style.animationPlayState = 'paused';
                });
                clearInterval(isMiss);
            }
        }
        if (gameStatus == 'over') {
            // Stop all balloon's animation
            balloons.forEach(balloon => {
                balloon.style.animationPlayState = 'paused';
            });
            clearInterval(isMiss);
        }
    }, 10);
    // // Update Score
    gameStatus = 'playing';
}
// Reset lever 
function resetLever() {
    let balloonsZone = document.querySelector('.balloon-zone');
    balloonsZone.innerHTML = '';
    arrayPosition = [];
    balloonExploded = 0;
}
// Check Character
function checkCharacterExist() {
    const balloons = document.querySelectorAll('.balloon');
    for (let i = balloons.length - 1; i >= 0; i--) {
        if (arr[arrayPosition[i]].vietnamese == strings &&
            balloons[i].getBoundingClientRect().top >= -50) {
            // Check Burst Balloon
            balloonBurst(balloons[i]);
            break;
        }
    }
}

/**
 * Function Support
 */
function createGreetings(lever) {
    let message = "";
    switch (lever) {
        case 1:
            message = "Good job, now is lever 1";
            break;
        case 2:
            message = "Great, lever 2 is coming";
            break;
        case 3:
            message = "Wow :O, how about lever 3";
            break;
        case 4:
            message = "Now we have lucky balloon, don't miss that :D. Lever 4";
            break;
        case 5:
            message = "And now we have unlucky balloon, please don't touch that =((. Lever 5";
            break;
        case 6:
            message = "Wow, maybe faster balloon speed can get shower, Lever 6";
            break;
        case 7:
            message = "Welcome to lever 7, Every get faster, good luck";
            break;
        case 8:
            message = "Welcome to endless lever, final lever =))";
            break;
        default:
            break;
    }
    return message;
}
function balloonBurst(balloon) {
    if (balloon.classList.contains('balloon-unlucky')) {
        const error = 'You have detonated a black balloon';
        setGameOver(error);
    } else {
        // Set Audio
        setAndPlayPronounce(arr[arrayPosition[balloon.id]].vietnamese);
        // Get new Number and replace position
        let num = randomInteger(0, arr.length - 1);
        while (checkSameNumber(num)) {
            num = randomInteger(0, arr.length - 1);
        }
        arrayPosition[balloon.id] = num;
        // Reset Strings and Update score
        strings = "";
        setMessage();
        score += balloon.classList.contains('balloon-lucky') ? 2 : 1;
        setScore();
        if(lever < 8){
            balloon.classList.contains('balloon-lucky') ? alertify.success('Wow you so Lucky, +2 Score') : alertify.success('+ 1 Score');
        }
        // Stop and reset Animation for balloon
        resetAnimationBalloon(balloon, num);
        // Processing Lever
        balloonExploded++;
        if (lever < 8) {
            const temp = leverPropertiesArray[lever].ballonNumberPassLever - leverPropertiesArray[lever].ballonNumber;
            if (balloonExploded == temp) {
                gameStatus = "prepareTransferLever";
            }
            checkResetLever();
        }
    }
}
// Check Reset Lever
function checkResetLever() {
    if (lever >= 8) {
        return;
    }
    if (balloonExploded == leverPropertiesArray[lever].ballonNumberPassLever) {
        gameStatus = 'transferLever';
        resetLever();
        lever++;
        setLever();
        // Show message Screen
        let screenMessage = document.querySelector('.screen-message');
        screenMessage.style.background = 'black';
        screenMessage.querySelector('div').style.color = 'white';
        screenMessage.style.border = 'none';
        screenMessage.style.display = 'initial';
        screenMessage.classList.add('fade-1');
        screenMessage.querySelector('div').innerHTML = createGreetings(lever);
        setTimeout(() => {
            document.body.style.background = `url("./public/image/backgroud-${lever}.jpg")`;
            document.body.style.backgroundSize = `cover`;
        }, 5000);
        setTimeout(() => {
            setUplever();
            // Remove
            screenMessage.style.display = 'none';
            screenMessage.classList.remove('fade-1');
        }, 10000);
    }
}
// Reset Animation for balloon
function resetAnimationBalloon(balloon, balloonWordNumber) {
    // Stop and reset Animation for balloon
    balloon.classList.remove("balloon-animation");
    if (gameStatus == "prepareTransferLever") {
        return;
    }
    // Set new Background Color for Balloon
    setRandomTypeBalloon(balloon, balloonWordNumber);
    // Only have time delay for the first time
    balloon.style.animationDelay = '0s';
    // Set Animation Duration based on lever
    if (balloon.classList.contains('balloon-lucky') ||
        balloon.classList.contains('balloon-unlucky')) {
        balloon.style.animationDuration = randomInteger(8, 10) + 's';
    } else {
        const min = leverPropertiesArray[lever].balloonSpellMin;
        const max = leverPropertiesArray[lever].balloonSpellMax;
        balloon.style.animationDuration = randomInteger(min, max) + 's';
    }
    // Add class Animation again
    setTimeout(() => {
        balloon.classList.add("balloon-animation");
    }, 100);
}
// Check miss balloon
function checkMissBalloon() {
    const balloons = document.querySelectorAll('.balloon');
    const message = document.querySelector('.message');
    for (let i = 0; i < balloons.length; i++) {
        if (Math.round(balloons[i].getBoundingClientRect().bottom) >= Math.round(message.getBoundingClientRect().bottom - message.offsetHeight)) {
            if (balloons[i].classList.contains('balloon-unlucky')) {
                // Get new Number and replace position
                let num = randomInteger(0, arr.length - 1);
                while (checkSameNumber(num)) {
                    num = randomInteger(0, arr.length - 1);
                }
                arrayPosition[i] = num;
                // Stop and reset Animation for balloon
                resetAnimationBalloon(balloons[i], num);
                balloonExploded++;
                checkResetLever();
                break;
            } else {
                return true;
            }
        }
    }
    return false;
}
// Set Score
function setScore() {
    const temp = document.querySelector('.score');
    temp.innerHTML = score;
}
// Set arrow Number
function setArrowNumber() {
    let temp = document.querySelector('.arrow-number');
    temp.innerHTML = arrowNumber;
}
// Set Spam
function setSpam() {
    const temp = document.querySelector('.spam');
    temp.innerHTML = spam;
}
// Set message
function setMessage() {
    const message = document.querySelector('.message');
    message.innerHTML = strings;
}
// Set message
function setLever() {
    const message = document.querySelector('.lever');
    message.innerHTML = lever;
    alertify.message(`Now is lever ${lever}`);
}
// Random Integer
function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
// Random Color (except Black and similar Black)
function randomColor() {
    var x = randomInteger(80, 255);
    var y = randomInteger(80, 255);
    var z = randomInteger(80, 255);
    return `rgb(${x},${y},${z})`;
}
// Set Random Black Balloon
function setRandomTypeBalloon(balloon, balloonWordNumber) {
    // Remove old style
    balloon.classList.remove('balloon-lucky');
    balloon.classList.remove('balloon-unlucky');
    const newBg = randomColor();
    /** 
     * 7 is lucky number, 4 is unlucky number
     * Lucky Balloon will appear in lever 4 and later
     * Unlucky Balloon will appear in lever 5 and later 
     * */
    randomBalloon = randomInteger(1, 7);
    // Lucky balloon
    if (randomBalloon == 7 && lever >= 4) {
        balloon.classList.add('balloon-lucky');
        balloon.style.backgroundColor = newBg;
        balloon.style.color = 'black';
        const starInnerHTML = '<i class="fas fa-star text-star"></i>';
        balloon.innerHTML = `${starInnerHTML}${arr[balloonWordNumber].japanese}
     <span class="text-vietnamese">${arr[balloonWordNumber].vietnamese}</span>`;
    }
    // Unlucky balloon
    else if (randomBalloon == 4 && lever >= 5) {
        balloon.classList.add('balloon-unlucky');
        balloon.style.backgroundColor = 'black';
        balloon.style.color = 'white';
        balloon.innerHTML = `${arr[balloonWordNumber].japanese}
     <span class="text-vietnamese">${arr[balloonWordNumber].vietnamese}</span>`;
    }
    // Norman balloon
    else {
        balloon.style.backgroundColor = newBg;
        balloon.style.color = 'black';
        balloon.innerHTML = `${arr[balloonWordNumber].japanese}
     <span class="text-vietnamese">${arr[balloonWordNumber].vietnamese}</span>`;
    }
    // Set box shadow for hover
    balloon.onmouseover = () => {
        balloon.style.boxShadow = `0px 0px 15px 15px ${balloon.style.backgroundColor}`;
    };
}
// Check same number Position in array Position
function checkSameNumber(number) {
    for (let i = 0; i < arrayPosition.length; i++) {
        if (arrayPosition[i] == number) {
            return true;
        }
    }
    return false;
}
// Set and Read character
function setAndPlayPronounce(vietnamese) {
    const audio = document.querySelector("#pronounce");
    audio.querySelector('source').src = `./public/audio/word/${vietnamese}.mp3`;
    audio.autoplay = true;
    audio.volume = 1;
    audio.load();
}
// Set Game Over
function setGameOver(error) {
    setGameOverCall++;
    if (setGameOverCall == 1) {
        // Open game over sound
        const sound = document.querySelector('#game-over-sound');
        sound.autoplay = true;
        sound.volume = 1;
        sound.load();
        gameStatus = "over";
        // Update Score
        updateScore();
        // Get Profile after play game
        setTimeout(() => {
            getProfile()
            .then((result) => {
                if (typeof (result) == 'object') {
                    // Append Message Game over
                    let screenMessage = document.querySelector('.screen-message');
                    screenMessage.style.background = 'gray';
                    screenMessage.classList.add('fade-2');
                    screenMessage.style.display = 'initial';
                    screenMessage.querySelector('div').innerHTML = `${error}<br>
                    <span class="small-text">Your score: ${score}</span><br>
                    <span class="small-text">Accumulated Score: ${result.accumulatedScore}</span><br>
                    <span class="small-text">Hightest score: ${result.hightestScore}</span><br>
                    <span class="small-text">Number of Arrows Remaining: ${result.arrowNumber}</span><br>
                    <a href="javascript:location.reload(true)" class="btn-screen-message btn-play-again">Play again</a>
                    <a href="./index.php" class="btn-screen-message btn-go-back">Go back menu</a>`;
                }
            });
        }, 2000);
        // Set error
        strings = error;
        setMessage();
        alertify.error(error);
        // Turn off bg music
        bgSound = false;
        const backgroundSound = document.querySelector('#background-sound');
        backgroundSound.autoplay = false;
        backgroundSound.load();
    }
}

// Arrow
let arrowButton = document.querySelector('.arrow-button');
arrowButton.onclick = () => {
    if (gameStatus == "playing" || gameStatus == "prepareTransferLever") {
        if (arrowNumber <= 0) {
            alertify.error("You've used up all the arrows");
        } else {
            if (!arrowButton.classList.contains('selected')) {
                arrowButton.classList.add('selected');
                arrowButton.style.backgroundColor = 'yellow';
                arrowButton.style.border = 'black 5px solid';
                // Set sound
                const audio = document.querySelector("#arrow-sound");
                audio.querySelector('source').src = `./public/audio/arrow/Arrow-Ready.mp3`;
                audio.autoplay = true;
                audio.volume = 0.5;
                audio.load();
            } else {
                arrowButton.classList.remove('selected');
                arrowButton.style.backgroundColor = 'Transparent';
                arrowButton.style.border = 'none';
            }
        }
    }
}

// Ajax update Score
async function updateScore() {
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
        score: score,
        arrowNumber: arrowNumber
    };
    const url = './server/updateScore.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (!result.isSuccess) {
        alert(result.error);
        logout();
    }
}
