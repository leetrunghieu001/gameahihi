const form = document.querySelector('form');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    formSubmit();
});
async function formSubmit() {
    let loader = document.querySelector('.loader');
    loader.style.display = 'initial';
    const email = document.querySelector('.email-input').value;
    const password = document.querySelector('.password-input').value;
    // Sent Data to Sever
    const data = {
        email: email,
        password: password
    };
    const url = './server/login.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        alert(`Welcome back, ${result.data.userName}`);
        localStorage.setItem('_token', result.data.token);
        window.location="./index.php";
    } else {
        alert(result.error);
    }
    // Close loader
    loader.style.display = 'none';
}