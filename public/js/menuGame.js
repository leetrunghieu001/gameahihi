window.onload = () => {
    let loader = document.querySelector('.loader');
    loader.style.display = 'inline';
    if (localStorage.getItem("_token") === null) {
        alert('Please Login First');
        window.location.href = "./login.php";
    }
    getProfile()
        .then((result) => {
            if (typeof (result) == 'object') {
                let info = document.querySelector('.hello i');
                info.innerHTML = result.fullname;
            }
        });
    loader.style.display = 'none';
}

// Func Press Button
function btnPress(value) {
    switch (value) {
        case 'newbie':
            window.location.href = "./game.php?newbie=true";
            break;
        case 'pro':
            window.location.href = "./game.php";
            break;
        case 'buy':
            window.location.href = "./shopping.php";
            break;
        case 'rank':
            window.location.href = "./topPlayer.php";
            break;
        default:
            logout();
            break;
    }
}

// Get Profile
async function getProfile() {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
    };
    const url = './server/getProfile.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        // Get profile
        return result.data
    } else {
        alert(result.error);
        logout();
    }
    return false;
}

// Logout
function logout() {
    localStorage.removeItem("_token");
    alert('You were logout');
    window.location.href = './login.php';
}