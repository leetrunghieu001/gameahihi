const form = document.querySelector('form');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    formSubmit(e);
});
async function formSubmit() {
    let loader = document.querySelector('.loader');
    loader.style.display = 'initial';
    const fullname = document.querySelector('.fullname').value;
    const email = document.querySelector('.email').value;
    const password = document.querySelector('.password').value;
    const rePassword = document.querySelector('.re-password').value;
    const gender = document.querySelector('.gender').value;
    // Sent Data to Sever
    const data = {
        fullname: fullname,
        email: email,
        password: password,
        rePassword: rePassword,
        gender: gender,
    };
    const url = './server/register.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        alert('Register Success, Now go to your email to confirm your account');
        let fn = document.querySelector('.fullname-hidden');
        let e = document.querySelector('.email-hidden');
        let dc = document.querySelector('.deactive-code-hidden');
        const btnSM = document.querySelector('.submit-hidden');
        fn.value = result.data.fullname;
        e.value = result.data.email;
        dc.value = result.data.deactiveCode;
        btnSM.click();
    } else {
        alert(result.error);
    }
    // Close loader
    loader.style.display = 'none';
}