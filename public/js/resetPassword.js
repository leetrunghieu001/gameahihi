const form = document.querySelector('.form-reset-password');
form.addEventListener('submit', (e) => {
    e.preventDefault();
    resetPassword();
});
async function resetPassword() {
    let loader = document.querySelector('.loader');
    loader.style.display = 'initial';
    const code = document.querySelector('.code-input').value;
    const password = document.querySelector('.password-input').value;
    const rePassword = document.querySelector('.re-password-input').value;
    // Sent Data to Sever
    const data = {
        code: code,
        password: password,
        rePassword: rePassword
    };
    const url = './server/resetPassword.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    console.log(result);
    if (result.isSuccess) {
        alert(result.data);
        window.location.href = "./login.php";
    } else {
        alert(result.error);
    }
    // Close loader
    loader.style.display = 'none';
}