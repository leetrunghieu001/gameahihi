window.onload = () => {
    let loader = document.querySelector('.loader');
    loader.style.display = 'inline';
    if (localStorage.getItem("_token") === null) {
        alert('Please Login First');
        window.location.href = "./login.php";
    }
    getProfile();
    loader.style.display = 'none';
}

// BuyArrow
function buyArrow(number) {
    switch (number) {
        case 1:
            setDialog(1,30);
            break;
        case 3:
            setDialog(3,20);
            break;
        case 10:
            setDialog(10,60);
            break;
        default:
            break;
    }
}
// Set dialog
function setDialog(number,price){
    let loader = document.querySelector('.loader');
    loader.style.display = 'inline';
    if (confirm(`Do you want to buy ${number} arrow with ${price} score ?`)) {
        buyingArrow(number);
      }
      loader.style.display = 'none';
}
// Get Profile
async function buyingArrow(number) {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
        arrowNumber: number
    };
    const url = './server/buyArrow.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if(result.isSuccess){
        alert(`Buy ${number} arrow success !`);
        document.querySelector('.user-score').innerHTML = result.data.newAccumulatedScore;
        document.querySelector('.user-arrow-number').innerHTML = result.data.newArrowNumber;
    }else{
        alert(result.error);
    }
}

// Get Profile
async function getProfile() {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
    };
    const url = './server/getProfile.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        // Get profile
        document.querySelector('.user-name').innerHTML = result.data.fullname;
        document.querySelector('.user-score').innerHTML = result.data.accumulatedScore;
        document.querySelector('.user-arrow-number').innerHTML = result.data.arrowNumber;
    } else {
        alert(result.error);
        logout();
    }
}

// Logout
function logout() {
    localStorage.removeItem("_token");
    alert('You were logout');
    window.location.href = './login.php';
}