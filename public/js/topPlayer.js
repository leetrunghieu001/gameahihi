window.onload = () => {
    let loader = document.querySelector('.loader');
    loader.style.display = 'inline';
    if (localStorage.getItem("_token") === null) {
        alert('Please Login First');
        window.location.href = "./login.php";
    }
    // Get profile
    getProfile()
        .then((result) => {
            if (typeof (result) == 'object') {
                let myInfo = document.querySelector('.my-info');
                const gender = result.gender == 1 ? 'Nam' : 'Nữ';
                myInfo.innerHTML = `<div class="xephang my-top"></div>
 <div class="tennhanvat">
     <p class="name"><i>${result.fullname}</i></p>
 </div>
 <div class="gioitinh">
     <p class="word"><i>${gender}</i></p>
 </div>
 <div class="diemso">
     <p class="word"><i>${result.hightestScore}</i></p>
 </div>`;
            }
        });
    setTimeout(() => {
        // Get index
        findIndex()
            .then((result) => {
                let myTop = document.querySelector('.my-top');
                if (myTop) {
                    if (result < 4) {
                        myTop.innerHTML = `<div class="icon-top${result}"></div>`;
                    } else {
                        myTop.innerHTML = `<div class="icon-top"><p>${result}</p></div>`;
                    }

                }
            });
        loader.style.display = `none`;
    }, 2000);

}

// Get Profile
async function getProfile() {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
    };
    const url = './server/getProfile.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    // console.log(result);
    if (result.isSuccess) {
        // Get profile
        return result.data
    } else {
        alert(result.error);
        logout();
    }
    return false;
}

// Find Index
async function findIndex() {
    // Find token value
    const token = localStorage.getItem("_token");
    // Sent Data to Sever
    const data = {
        token: token,
    };
    const url = './server/findIndex.php';
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(data)
    });
    // Get Result
    const result = await response.json();
    if (result.isSuccess) {
        return result.data;
    } else {
        alert(result.error);
        logout();
    }
    return false;
}

// Logout
function logout() {
    localStorage.removeItem("_token");
    alert('You were logout');
    window.location.href = './login.php';
}