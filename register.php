<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="./public/css/register.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <div class="background">
        <div class="main">
            <h1 class="dangki">Đăng Kí</h1>
            <form>
                <div class="dangki-input">
                    <input class="input-dangki fullname" placeholder="Your Full Name" type="text" name="fullname" autocomplete="off" required>
                </div>
                <div class="dangki-input">
                    <input class="input-dangki email" placeholder="Your Email" type="email" name="email" autocomplete="off" required>
                </div>
                <div class="dangki-input">
                    <input class="input-dangki password" placeholder="Your Password" type="password" name="password" autocomplete="off" required>
                </div>
                <div class="dangki-input">
                    <input class="input-dangki re-password" placeholder="Confirm password" type="password" name="re-password" autocomplete="off" required>
                </div>
                <div class="custom-select">
                    <select class="combobox-dangki gender" name="gender">
                        <option value="1" selected disabled hidden>Gender</option>
                        <option value="1">Nam</option>
                        <option value="1">Nữ</option>
                    </select>
                </div>
                <div class="submit-dangki">
                    <button class="submit" type="submit">Đăng Kí</button>
                </div>
            </form>
            <div class="submit-dangki">
                <a href="./login.php">
                    <button class="submit">&#8592; Đăng Nhập</button>
                </a>
            </div>
        </div>
    </div>
    <div class="form-hidden" style="display: none;">
        <form method="POST" action="./server/sendMailConfirm.php">
            <input class="fullname-hidden" name="fullname" type="text">
            <input class="email-hidden" name="email" type="text">
            <input class="deactive-code-hidden" name="deactiveCode" type="text">
            <button class="submit-hidden" type="submit" name="register-submit"></button>
        </form>
    </div>
    <!-- Loader -->
    <div class="loader">
        <div class="load"></div>
    </div>
    <script src="./public/js/checkNotLogin.js"></script>
    <script src="./public/js/register.js"></script>
</body>

</html>