<?php
require_once './config/database.php';
require_once './config/config.php';
spl_autoload_register(function ($class_name) {
    require './app/models/' . $class_name . '.php';
});
$userModel = new UserModel();
if(isset($_GET['email'])){
    $email = $_GET['email'];
    if(!$userModel->checkEmailExit($email)){
        echo "<script>
        alert(\"Opps, Some thing Wrong at this email !!\");
        window.location.href = \"./login.php\";
    </script>";
    }
}else{
    header("location: ./login.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forget Password</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="./public/css/login.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <div class="background">
        <div class="main">
            <h1 class="dangnhap">Forget Password</h1>
            <h3 style="color: white;">Enter your code and your new password</h3>
            <form class="form-reset-password">
                <div class="dangnhap-input">
                    <i class="fas fa-user-tie fa-4x"></i>
                    <input class="input-dangnhap email-input" disabled type="email" name="email" autocomplete="off" required value="<?= $email ?>">
                </div>
                <div class="dangnhap-input">
                    <i class="fas fa-key fa-4x" style="color: white;"></i>
                    <input class="input-dangnhap code-input" placeholder="Your Reset Code" type="text" name="code" autocomplete="off" required>
                </div>
                <div class="dangnhap-input">
                    <i class="fas fa-lock fa-4x"></i>
                    <input class="input-dangnhap password-input" placeholder="Your Password" type="password" name="password" autocomplete="off" required>
                </div>
                <div class="dangnhap-input">
                    <i class="fas fa-lock fa-4x"></i>
                    <input class="input-dangnhap re-password-input" placeholder="Your Confirm Password" type="password" name="re-password" autocomplete="off" required>
                </div>
                <div class="submit-dangnhap">
                    <button class="submit btn-submit" type="submit">RESET PASSWORD</button>
                </div>
            </form>
            <div class="submit-dangki">
                <a href="./login.php">
                    <button class="submit">LOGIN</button>
                </a>
            </div>
        </div>
    </div>
    <div class="loader">
        <div class="load"></div>
    </div>
    <script src="./public/js/checkNotLogin.js"></script>
    <script src="./public/js/resetPassword.js"></script>
</body>

</html>