<?php
require_once '../config/database.php';
require_once '../config/config.php';
spl_autoload_register(function ($class_name) {
    require '../app/models/' . $class_name . '.php';
});

$input = json_decode(file_get_contents('php://input'), true);
$code = $input['code'];
$password = $input['password'];
$rePassword = $input['rePassword'];
$userModel = new UserModel();
$check = $userModel->resetPasswordByCode($code,$password,$rePassword);
$data = [
    "isSuccess" => $check->isSuccess,
    "data" => $check->data,
    "error" => $check->error
];
echo json_encode($data);
