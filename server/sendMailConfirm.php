<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sending Mail</title>
    <link rel="icon" href="../public/image/icon.jpg" type="image/x-icon">
</head>
<style>
    /* Loader */
    .loader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: gray;
    }

    .load {
        position: absolute;
        top: 40%;
        left: 45%;
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>

<body>
    <div class="loader">
        <div class="load"></div>
    </div>
    <?php
    if (isset($_POST['register-submit'])) {
        echo $_SERVER["PHP_SELF"];
        $fullname = $_POST['fullname'];
        $userEmail = $_POST['email'];
        $urlActive = getURL().'/../activeAccount.php?code='.$_POST['deactiveCode'];
        $body = "<!DOCTYPE html>
        <html xmlns=\"http://www.w3.org/1999/xhtml\">

        <head>
          <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
          <title>Verify your email address</title>
          <style type=\"text/css\" rel=\"stylesheet\" media=\"all\">
            /* Base ------------------------------ */
            *:not(br):not(tr):not(html) {
              font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
            }

            body {
              width: 100% !important;
              height: 100%;
              margin: 0;
              line-height: 1.4;
              background-color: #F5F7F9;
              color: #839197;
              -webkit-text-size-adjust: none;
            }

            a {
              color: #414EF9;
            }

            /* Layout ------------------------------ */
            .email-wrapper {
              width: 100%;
              margin: 0;
              padding: 0;
              background-color: #F5F7F9;
            }

            .email-content {
              width: 100%;
              margin: 0;
              padding: 0;
            }

            /* Masthead ----------------------- */
            .email-masthead {
              padding: 25px 0;
              text-align: center;
            }

            .email-masthead_logo {
              max-width: 400px;
              border: 0;
            }

            .email-masthead_name {
              font-size: 16px;
              font-weight: bold;
              color: #839197;
              text-decoration: none;
              text-shadow: 0 1px 0 white;
            }

            /* Body ------------------------------ */
            .email-body {
              width: 100%;
              margin: 0;
              padding: 0;
              border-top: 1px solid #E7EAEC;
              border-bottom: 1px solid #E7EAEC;
              background-color: #FFFFFF;
            }

            .email-body_inner {
              width: 570px;
              margin: 0 auto;
              padding: 0;
            }

            .email-footer {
              width: 570px;
              margin: 0 auto;
              padding: 0;
              text-align: center;
            }

            .email-footer p {
              color: #839197;
            }

            .body-action {
              width: 100%;
              margin: 30px auto;
              padding: 0;
              text-align: center;
            }

            .body-sub {
              margin-top: 25px;
              padding-top: 25px;
              border-top: 1px solid #E7EAEC;
            }

            .content-cell {
              padding: 35px;
              border: solid 1px black;
            }

            .align-right {
              text-align: right;
            }

            /* Type ------------------------------ */
            h1 {
              margin-top: 0;
              color: #292E31;
              font-size: 19px;
              font-weight: bold;
              text-align: left;
            }

            h2 {
              margin-top: 0;
              color: #292E31;
              font-size: 16px;
              font-weight: bold;
              text-align: left;
            }

            h3 {
              margin-top: 0;
              color: #292E31;
              font-size: 14px;
              font-weight: bold;
              text-align: left;
            }

            p {
              margin-top: 0;
              color: #839197;
              font-size: 16px;
              line-height: 1.5em;
              text-align: left;
            }

            p.sub {
              font-size: 12px;
            }

            p.center {
              text-align: center;
            }

            /* Buttons ------------------------------ */
            .button {
              display: inline-block;
              width: 200px;
              background-color: #414EF9;
              border-radius: 3px;
              color: white;
              font-size: 15px;
              line-height: 45px;
              text-align: center;
              text-decoration: none;
              -webkit-text-size-adjust: none;
              mso-hide: all;
            }

            .button--green {
              background-color: #28DB67;
            }

            .button--red {
              background-color: #FF3665;
            }

            .button--blue,
            .button--blue:hover,
            .button--blue:visited {
              background-color: skyblue;
              color: white;
            }

            /*Media Queries ------------------------------ */
            @media only screen and (max-width: 600px) {

              .email-body_inner,
              .email-footer {
                width: 100% !important;
              }
            }

            @media only screen and (max-width: 500px) {
              .button {
                width: 100% !important;
              }
            }
          </style>
        </head>

        <body>
          <table class=\"email-wrapper\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
            <tr>
              <td align=\"center\">
                <table class=\"email-content\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                  <!-- Logo -->
                  <tr>
                    <td class=\"email-masthead\">
                      <a class=\"email-masthead_name\">Verify your email address</a>
                    </td>
                  </tr>
                  <!-- Email Body -->
                  <tr>
                    <td class=\"email-body\" width=\"100%\">
                      <table class=\"email-body_inner\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">
                        <!-- Body content -->
                        <tr>
                          <td class=\"content-cell\">
                            <h1>Verify your email address</h1>
                            <p>Hi <b>" . $fullname . "</b></p>
                            <p>We noticed that you have just registered an account at our \"Hiragana game\" website using this email address
                            </p>
                            <p>If you did, click the confirmation link below to verify your account</p>
                            <p>Welcome to out website, Wish you have a good experience</p>
                            <!-- Action -->
                            <table class=\"body-action\" align=\"center\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                              <tr>
                                <td align=\"center\">
                                  <div>
                                    <a href=\"". $urlActive . "\" class=\"button button--blue\">Verify Email</a>
                                  </div>
                                </td>
                              </tr>
                            </table>
                            <p>Thanks,<br>The Canvas Team</p>
                            <!-- Sub copy -->
                            <table class=\"body-sub\">
                              <tr>
                                <td>
                                  <p class=\"sub\">If you’re having trouble clicking the button, copy and paste the URL below into
                                    your web browser.
                                  </p>
                                  <p class=\"sub\"><a href=\"".$urlActive."\">" .$urlActive. "</a></p>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table class=\"email-footer\" align=\"center\" width=\"570\" cellpadding=\"0\" cellspacing=\"0\">
                        <tr>
                          <td class=\"footer\">
                            <br class=\"sub center\">
                            <p>TEAM 7</p>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </body>

        </html>";
        //SMTP needs accurate times, and the PHP time zone MUST be set
        //This should be done in your php.ini, but this is how to do it if you don't have access to that
        date_default_timezone_set('Etc/UTC');

        require './smtpmail/PHPMailerAutoload.php';

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        $mail->CharSet = "UTF-8";

        //Tell PHPMailer to use SMTP
        $mail->isSMTP();

        //Enable SMTP debugging
        // 0 = off (for production use)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug = 2;

        //Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';

        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls';

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "emailusetosendonly@gmail.com";

        //Password to use for SMTP authentication
        $mail->Password = "324Hoanghuunam";

        //Set who the message is to be sent from
        $mail->setFrom('emailusetosendonly@gmail.com', 'Confirm Account');

        //Set an alternative reply-to address
        $mail->addReplyTo('emailusetosendonly@gmail.com', 'Confirm Account');

        //Set who the message is to be sent to
        $mail->addAddress($userEmail, $fullname);

        //Set the subject line
        $mail->Subject = 'Confirm Account';

        //Read an HTML message body from an external file, convert referenced images to embedded,
        //convert HTML into a basic plain-text alternative body
        $mail->msgHTML($body);

        //Replace the plain text body with one created manually
        // $mail->AltBody = 'This is a plain-text message body';

        //Attach an image file
        // $mail->addAttachment('images/phpmailer_mini.png');

        //send the message, check for errors
        if (!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }
    // Get URL func
    function getURL()
    {
        $pageURL = 'http';
        if (!empty($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == 'on') {
                $pageURL .= "s";
            }
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
    echo "<script>window.location.href = \"../login.php\";</script>";
    ?>
</body>

</html>