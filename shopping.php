<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shopping</title>
    <link rel="stylesheet" href="./public/css/shopping.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
</head>

<body>
    <div class="background">
        <div class="main">
            <a href="./index.php" class="back-to-menu-game">&#8592; Back</a>
            <h1 class="muasam">Arrow Shopping</h1>
            <div class="user-info">
                <div>Hi "<i class="user-name"></i>"</div>
                <div>Your Accumulated Score: <i class="user-score"></i></div>
                <div>Your Arrow Number: <i class="user-arrow-number"></i></div>
            </div>
            <div class="box-shopping">
                <div class="image-shopping"></div>
                <div class="all-item">
                    <div class="item-shopping">
                        <div class="image-1-arrow"></div>
                        <div class="content-cung">
                            <h1>1 Arrow</h1>
                            <h1>(Not Sale)</h1>
                            <span>Price : 10 Score</span>
                        </div>
                        <div class="submit-muasam">
                            <button class="submit" type="submit" onclick="buyArrow(1)">Buy</button>
                        </div>
                    </div>
                    <div class="item-shopping">
                        <div class="image-3-arrow"></div>
                        <div class="content-cung">
                            <h1>3 Arrow</h1>
                            <h1>(1 free)</h1>
                            <span>Price : 20 Score</span>
                        </div>
                        <div class="submit-muasam">
                            <button class="submit" type="submit" onclick="buyArrow(3)">Buy</button>
                        </div>
                    </div>
                    <div class="item-shopping">
                        <div class="image-10-arrow"></div>
                        <div class="content-cung">
                            <h1>10 Arrow</h1>
                            <h1>(4 free)</h1>
                            <span>Price : 60 Score</span>
                        </div>
                        <div class="submit-muasam">
                            <button class="submit" type="submit" onclick="buyArrow(10)">Buy</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Loader -->
    <div class="loader"><div class="load"></div></div>
    <script src="./public/js/shopping.js"></script>
</body>

</html>