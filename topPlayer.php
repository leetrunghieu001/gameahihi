<?php
require_once './config/database.php';
require_once './config/config.php';
spl_autoload_register(function ($class_name) {
    require './app/models/' . $class_name . '.php';
});
$userModel = new UserModel();
$topPlayer = $userModel->getTopPlayer();
$stt = 1;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rank</title>
    <link rel="icon" href="./public/image/icon.jpg" type="image/x-icon">
    <link rel="stylesheet" href="./public/css/bxh.css">
</head>

<body>
    <div class="background">

        <div class="btn-go-back"><a href="./index.php">Go back</a></div>
        <div class="header">
            <div>Xếp Hạng</div>
            <div>Tên Nhân Vật</div>
            <div>Giới Tính</div>
            <div class="diemso1">Điểm Số</div>
        </div>
        <div class="main">
            <?php foreach ($topPlayer as $player) { ?>
                <div class="row">
                    <div class="xephang">
                        <div class="icon-top<?= $stt > 3 ? '' : $stt ?>">
                            <p><?= $stt > 3 ? $stt : '' ?></p>
                        </div>
                    </div>
                    <div class="tennhanvat">
                        <p class="name"><?= $player['fullname'] ?></p>
                    </div>
                    <div class="gioitinh">
                        <p class="word"><?= $player['gender'] == 1 ? 'Nam' : 'Nữ' ?></p>
                    </div>
                    <div class="diemso">
                        <p class="word"><?= $player['hightestScore'] ?></p>
                    </div>
                </div>
            <?php
                $stt++;
                if ($stt == 6) {
                    break;
                }
            } ?>
            <div class="row my-info"></div>
        </div>
    </div>
    <div class="loader"><div class="load"></div></div>
    <script src="./public/js/topPlayer.js"></script>
</body>

</html>